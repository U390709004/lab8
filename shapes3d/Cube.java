package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    private double depth ;

    public Cube(){
        super();
        this.depth = 10;
    }
    public Cube(double sideLength){
        super(sideLength);
        this.depth = sideLength;

    }
    public double getCubeArea()

    {

        return(6*super.getArea());

    }



    public double getCubeVolume()

    {

        return(super.getArea()*depth);

    }
    public String toString() {
        return String.format ("A Cube with area = " + getCubeArea()+", volume = " + getCubeVolume());
    }
}

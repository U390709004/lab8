package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    private double height;

    public Cylinder() {
        super();
        this.height = 10.0;
    }

    public Cylinder(double radius, double height) {
        this.height = height;
        super.setRadius(radius);

    }
    public void setHeight(double h)
    {
        if (h >= 0)
            height = h;
        else
            height = 0;
    }

    public double getCylinderArea()
    {
        return 2 * super.getArea() + 2 * Math.PI * getRadius() * height;
    }


    public double getCylinderVol() {
        return height * Math.pow(super.getRadius(), 2) * Math.PI;

    }
        public String toString() {
        return String.format ("A Cylinder with area = " + getCylinderArea()+", volume = " + getCylinderVol());
    }
}

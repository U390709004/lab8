package shapes2d;

public class Square {
    private double sideLength;

    public Square() {
        this(10.0);
    }

    public Square(double sideLength) {
        this.sideLength = sideLength;
    }

    public double getSideLength() {
        return sideLength;
    }

    public void setSideLength(double sideLength) {
        this.sideLength = sideLength;
    }

    public double getArea() {
        return Math.pow(getSideLength(), 2);
    }
    public double getPerimeter(){
        return 4 * getSideLength();
    }

    public String toString() {
        return String.format ("A Square with side = " + getSideLength()+ ", area = "+ getArea()+", perimeter: " + getPerimeter());
    }

}
